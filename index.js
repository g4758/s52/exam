function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.

    //Conditons:
        // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        // If letter is invalid, return undefined.
     if (letter.length == 1) {
        for (let i = 0; i <= sentence.length; i++) {
            if(sentence[i] == letter){
                result++;
             }
        } return result;

     } else {
            return undefined
        }
}

function isIsogram(text) {  
    
        // An isogram is a word where there are no repeating letters.

    //Check:
        // The function should disregard text casing before doing anything else.


    //Condition:
        // If the function finds a repeating letter, return false. Otherwise, return true.
    let newText = text.toLowerCase()
    let myArray = newText.split(``)
    const toFindDuplicates = arry => arry.filter((item, index) => arry.indexOf(item) !== index)
    const duplicateElements = toFindDuplicates(myArray);
    console.log(duplicateElements);
        if(duplicateElements.length>=1){
            return false
        }else{
            return true
        }
    }



function purchase(age, price) {

    //Conditions:
        // Return undefined for people aged below 13.
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        // Return the rounded off price for people aged 22 to 64.

    //Check:
        // The returned value should be a string.
    if( age < 13) {
        return undefined

    } else if( age >= 13 && age <= 21) {

        let discountPrice = price * 0.8
        let rounded = discountPrice.toFixed(2)
        let result = rounded.toString()

        return result

    } else if(age >= 22 && age <= 64) {

        let discountPrice = price.toFixed(2)
        let result = discountPrice.toString()

        return result

    } else if ( age > 64 ) {

        let discountPrice = price * 0.8
        let rounded = discountPrice.toFixed(2)
        let result = rounded.toString()

        return result

    } else {

        return price.toFixed(2);
    }
}

function findHotCategories(items) {
    //Goal:
        // Find categories that has no more stocks.
        // The hot categories must be unique; no repeating categories.

    //Array for the test:
        // The passed items array from the test are the following:
        // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
        // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
        // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
        // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
        // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }


    //Expected return must be array:
        // The expected output after processing the items array is ['toiletries', 'gadgets'].

    // Note:
        // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    let Ctgry = items.filter(( items )=>{
        
        let {stocks,category} = items
            if(stocks == 0){
                return category
        }
    })

    let uniqueCtgry = [...new Set( Ctgry ) ]
    uniqueCtgry = uniqueCtgry.filter(function( element ) {
        return element !== undefined;

    });
        return uniqueCtgry
    }
// let myItems = [
//         { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
//         { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
//         { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
//         { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
//         { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
// ]
// findHotCategories(myItems)

function findFlyingVoters(candidateA, candidateB) {
    //Goal:
        // Find voters who voted for both candidate A and candidate B.

    //Array for the test:    
        // The passed values from the test are the following:
        // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
        // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // Expected return must be array:
        // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].

    //Note:
        // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    let flyingVoters = [];

        for ( let i = 0; i <= candidateA.length-1; i++ ){

        console.log(`candidate A: ${i}: ${candidateA[i]}`)
        
        for ( let c = 0; c <= candidateB.length-1; c++ ){
            console.log(`candidate B: ${c}: ${candidateB[c]}`)

         if (candidateA[i] == candidateB[c]){

                flyingVoters.push(candidateB[c]);
            }
        }
    }
    
    return flyingVoters;

    }
// let A = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
// let B = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

// findFlyingVoters(A,B)
module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};
